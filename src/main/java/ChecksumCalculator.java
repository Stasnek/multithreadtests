import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

public class ChecksumCalculator implements Callable<Map<String, String>> {
    Map<String, String> filesChecksum = new HashMap<String, String>();
    Collection<File> files;

    public ChecksumCalculator(Collection<File> files) {
        this.files = files;
    }

    public Map<String, String> calculateFolderCheckSumMultiThread() {
        filesChecksum = new HashMap<String, String>();
        ExecutorService executorService = Executors.newCachedThreadPool();
        int threadCount = Runtime.getRuntime().availableProcessors();
        List<File> list = new ArrayList(files);
        int filesToThread = files.size() / threadCount + (files.size() % threadCount == 0? 0: 1);
        int startIndex;
        int endIndex;
        List<Future<Map<String, String>>> futures = new ArrayList<Future<Map<String, String>>>();
        for (int i = 0; i < threadCount; i++) {
            startIndex = filesToThread * i;
            endIndex = (startIndex + filesToThread) < files.size() ? startIndex + filesToThread : files.size();
            System.out.println(" thread " + i + " startIndex " + startIndex + " endIndex " + endIndex);
            futures.add(executorService.submit(new ChecksumCalculator(list.subList(startIndex, endIndex))));
        }
        int a = 0;
        for (Future<Map<String, String>> future : futures) {
            try {
                System.out.println(" thread " + a + " size " + future.get().size());
                a++;
                filesChecksum.putAll(future.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } finally {
                executorService.shutdown();
            }
        }// 850,5
        return filesChecksum;
    }

    public Map<String, String> calculateFolderCheckSum() {
        Map<String, String> filesChecksum = new HashMap<String, String>();
        for (File file : files) {
            filesChecksum.put(file.getPath(), calculateFileChecksum(file));
        }
        return filesChecksum;
    }

    public String calculateFileChecksum(File file) {
        FileInputStream fis = null;
        String md5 = "";
        try {
            fis = new FileInputStream(file);
            md5 = DigestUtils.md5Hex(fis);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            org.apache.commons.io.IOUtils.closeQuietly(fis);
            return md5;
        }
    }

    public void run() {
        calculateFolderCheckSum();
    }

    public Map<String, String> call() throws Exception {
        return calculateFolderCheckSum();
    }
}
