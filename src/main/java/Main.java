import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Collection<File> files = FileUtils.listFiles(new File(
                "C:\\workspace\\test"), TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);

        ChecksumCalculator checksumCalculator = new ChecksumCalculator(files);
        double startTime = System.currentTimeMillis();
        Map<String, String> checksums = checksumCalculator
                .calculateFolderCheckSum();
        double endTime = (System.currentTimeMillis() - startTime) / 1000;
        for (Map.Entry<String, String> entry : checksums.entrySet()) {
            System.out.println(" File: " + entry.getKey() + " hash: " + entry.getValue());
        }

        checksumCalculator = new ChecksumCalculator(files);
        double startTimeMulti = System.currentTimeMillis();
        Map<String, String> checksumsMulti = checksumCalculator.calculateFolderCheckSumMultiThread();
        double endTimeMulti = (System.currentTimeMillis() - startTimeMulti) / 1000;

        System.out.println("checksums size: " + checksums.size());
        System.out.println("Multi checksums size: " + checksumsMulti.size());

        System.out.println("Calculated in " + endTime + " seconds.");
        System.out.println("Multi calculated in " + endTimeMulti + " seconds.");

        MapDifference<String, String> difference = Maps.difference(checksums, checksumsMulti);
        System.out.println(difference);
    }

}
